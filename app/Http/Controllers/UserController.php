<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use Hash;
Use App\User;
use App\Role;
use Session;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function index(){
    	if(Session::get('dataku')==NULL) return redirect('./login');
        else{
            $data = User::join('com_role','com_user.role_cdFK','com_role.role_cd')->get();
            return view('User/tampil',compact('data'));
        }
    }
    public function tambah(){
    	$data = User::orderby('user_id','desc')->first();
    	$data2 = substr($data->user_id,0,3)+1;
    	if($data2>999)
    	$id = str_pad($data2, 4, '0', STR_PAD_LEFT);
    	else
    	$id = str_pad($data2, 3, '0', STR_PAD_LEFT);
        $role = Role::all();
    	return view('User/tambah',compact('id', 'role'));
    }
    public function tambahkeun(Request $request){
    	$data = new User;
    	$data->user_id = $request->user_id;
    	$data->user_nm = $request->user_nm;
    	$data->password = $request->password;
        $data->role_cdFK = $request->level;
    	$data->emp_id = 0;
    	$data->save();
    }
    public function edit($user_id){
        $id = str_pad($user_id, 3, '0', STR_PAD_LEFT);
        $data=User::find($id."T");
        $role = Role::all();
        return view('User/edit',compact('data','role','user_id'));
    }
    public function update(Request $request){
        $data = User::join('com_role','com_user.role_cdFK','com_role.role_cd')->get();
        $dataq=User::where('user_id',(str_pad($request->user_id, 3, '0', STR_PAD_LEFT)."T"))->firstOrFail();
        $dataq->user_nm = $request->user_nm;
        $dataq->password = $request->password;
        $dataq->role_cdFK = $request->level;
        $dataq->emp_id = 0;
        $dataq->save();
        return redirect('./');
    }
    public function destroy($user_id){
        $data=User::where('user_id',(str_pad($user_id, 3, '0', STR_PAD_LEFT)."T"))->firstOrFail()->delete();
        return redirect('./');
    }
    public function login(){
        return view('User/login');
    }
    public function loginPost(Request $request){
        $data = User::where('user_id',$request->user_id)->where('password',$request->password)->first();
        if($data==NULL){
            return redirect('./login');
        }else{
            Session::put('dataku',$data);
            return redirect('./');
        }
    }

    public function logout(){
        Session()->flush();
        return view('User/login');
    }

}
