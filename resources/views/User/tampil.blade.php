<table>
	<thead>
		<tr>
			<th>No</th>
			<th>Nama</th>
			<th>Username</th>
			<th>Level</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
	<?php $no=1; ?>
		@foreach($data as $val)
		<tr>
			<td>{{ $no++ }}</td>
			<td>{{ $val->user_nm }}</td>
			<td>{{ $val->user_id }}</td>
			<td>{{ $val->role_nm }}</td>
			<td><a href="edit/{{$val->user_id}}">Edit</a> <a href="destroy/{{$val->user_id}}">Hapus</a></td>
		</tr>
		@endforeach
	</tbody>
</table>
<a href="tambah">Tambah User</a>
<a href="./logout">LogOut User</a>