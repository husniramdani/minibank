-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 19, 2018 at 11:04 AM
-- Server version: 5.6.26
-- PHP Version: 5.5.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `minibanksv1`
--

-- --------------------------------------------------------

--
-- Table structure for table `com_code`
--

CREATE TABLE IF NOT EXISTS `com_code` (
  `com_cd` varchar(20) NOT NULL,
  `code_nm` varchar(100) DEFAULT NULL,
  `code_group` varchar(20) DEFAULT NULL,
  `code_value` varchar(100) DEFAULT NULL,
  `modi_id` varchar(20) DEFAULT NULL,
  `modi_datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `com_code`
--

INSERT INTO `com_code` (`com_cd`, `code_nm`, `code_group`, `code_value`, `modi_id`, `modi_datetime`) VALUES
('KODE_00', 'Login', 'ACTIVITY_USER', 'ACTIVITY_USER_00', NULL, NULL),
('KODE_01', 'Logout', 'ACTIVITY_USER', 'ACTIVITY_USER_01', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `com_log_data`
--

CREATE TABLE IF NOT EXISTS `com_log_data` (
  `log_id` int(11) NOT NULL,
  `table_nm` int(11) NOT NULL,
  `table_pk` int(11) NOT NULL,
  `change_tp` int(11) NOT NULL,
  `change_ds` int(11) NOT NULL,
  `log_date` int(11) NOT NULL,
  `data_before` int(11) NOT NULL,
  `data_after` int(11) NOT NULL,
  `modi_id` int(11) NOT NULL,
  `modi_datetime` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `com_log_detail`
--

CREATE TABLE IF NOT EXISTS `com_log_detail` (
  `log_id` int(11) NOT NULL,
  `column_nm` int(11) NOT NULL,
  `old_value` int(11) NOT NULL,
  `new_value` int(11) NOT NULL,
  `change_tp` int(11) NOT NULL,
  `change_ds` int(11) NOT NULL,
  `modi_id` int(11) NOT NULL,
  `modi_datetime` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `com_log_login`
--

CREATE TABLE IF NOT EXISTS `com_log_login` (
  `seq_no` bigint(20) NOT NULL,
  `activity_user` varchar(20) DEFAULT NULL,
  `activity_datetime` datetime DEFAULT NULL,
  `modi_id` varchar(20) DEFAULT NULL,
  `modi_datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `com_menu`
--

CREATE TABLE IF NOT EXISTS `com_menu` (
  `menu_cd` varchar(20) NOT NULL,
  `menu_nm` varchar(20) DEFAULT NULL,
  `menu_root` varchar(20) DEFAULT NULL,
  `menu_url` varchar(50) DEFAULT NULL,
  `menu_no` varchar(10) DEFAULT NULL,
  `menu_level` int(11) DEFAULT NULL,
  `menu_image` varchar(100) DEFAULT NULL,
  `menu_tp` char(1) DEFAULT NULL,
  `menu_param` varchar(20) DEFAULT NULL,
  `active_st` char(1) DEFAULT NULL,
  `modi_id` varchar(20) DEFAULT NULL,
  `modi_datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `com_role`
--

CREATE TABLE IF NOT EXISTS `com_role` (
  `role_cd` varchar(20) NOT NULL,
  `role_nm` varchar(100) DEFAULT NULL,
  `rule_tp` varchar(4) DEFAULT NULL,
  `modi_id` varchar(20) DEFAULT NULL,
  `modi_datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `com_role`
--

INSERT INTO `com_role` (`role_cd`, `role_nm`, `rule_tp`, `modi_id`, `modi_datetime`) VALUES
('R001', 'Admin', '1111', '1', '2018-05-16 07:54:20'),
('R002', 'Supervisor', '1110', '1', '2018-05-16 07:54:41'),
('R003', 'Teller', '1110', '1', '2018-05-16 07:55:19'),
('R004', 'Customer Service', '1110', '1', '2018-05-16 07:55:40');

-- --------------------------------------------------------

--
-- Table structure for table `com_role_menu`
--

CREATE TABLE IF NOT EXISTS `com_role_menu` (
  `role_menu_id` int(11) NOT NULL,
  `role_cdFK` varchar(20) NOT NULL,
  `menu_cdFK` varchar(20) DEFAULT NULL,
  `rule_tp` varchar(4) DEFAULT NULL,
  `modi_id` varchar(20) DEFAULT NULL,
  `modi_datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `com_user`
--

CREATE TABLE IF NOT EXISTS `com_user` (
  `user_id` varchar(4) NOT NULL,
  `user_nm` varchar(50) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `login_st` char(1) DEFAULT NULL,
  `lock_st` char(1) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `rule_tp` varchar(4) DEFAULT NULL,
  `role_cdFK` varchar(20) NOT NULL,
  `emp_id` bigint(20) NOT NULL,
  `modi_id` varchar(20) DEFAULT NULL,
  `modi_datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `com_user`
--

INSERT INTO `com_user` (`user_id`, `user_nm`, `password`, `login_st`, `lock_st`, `last_login`, `rule_tp`, `role_cdFK`, `emp_id`, `modi_id`, `modi_datetime`) VALUES
('001T', 'Amin', 'Amin', '1', '0', NULL, '1110', 'R001', 1, '1', '2018-05-16 08:06:21'),
('002T', 'Aman', 'Aman', '1', '0', NULL, '1110', 'R003', 2, '1', '2018-05-16 08:10:10');

-- --------------------------------------------------------

--
-- Table structure for table `trx_account`
--

CREATE TABLE IF NOT EXISTS `trx_account` (
  `account_id` bigint(20) NOT NULL,
  `cif_idFK` bigint(20) DEFAULT NULL,
  `branch_idFK` bigint(20) NOT NULL,
  `accounttp_cdFK` varchar(20) DEFAULT NULL,
  `account_no` varchar(20) DEFAULT NULL,
  `saldo` int(11) DEFAULT NULL,
  `address` varchar(200) NOT NULL,
  `rt` int(11) NOT NULL,
  `rw` int(11) NOT NULL,
  `region_kel` varchar(100) NOT NULL,
  `region_kec` varchar(100) NOT NULL,
  `region_kab` varchar(100) NOT NULL,
  `region_prop` varchar(100) NOT NULL,
  `postcode` varchar(20) NOT NULL,
  `purpose_open` varchar(20) NOT NULL,
  `sourcein_tp` varchar(4) NOT NULL,
  `sourceout_tp` varchar(3) NOT NULL,
  `freqin_tp` varchar(5) NOT NULL,
  `freqout_tp` varchar(5) NOT NULL,
  `created_user` varchar(20) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `approved_user` varchar(20) DEFAULT NULL,
  `approved_date` datetime DEFAULT NULL,
  `currency_cd` varchar(20) DEFAULT NULL,
  `modi_id` varchar(20) DEFAULT NULL,
  `modi_datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trx_account_trx`
--

CREATE TABLE IF NOT EXISTS `trx_account_trx` (
  `trx_id` varchar(3) NOT NULL,
  `account_idFK` bigint(20) NOT NULL,
  `trxtp_cd` float NOT NULL,
  `saldo_start` int(11) NOT NULL,
  `saldo_trx` int(11) NOT NULL,
  `saldo` int(11) NOT NULL,
  `penyetor_nm` varchar(50) DEFAULT NULL,
  `penyetor_idcard_tp` varchar(20) DEFAULT NULL,
  `penyetor_idcard_no` int(11) DEFAULT NULL,
  `berita_setoran` varchar(200) DEFAULT NULL,
  `process_user` varchar(20) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `approved_user` varchar(20) DEFAULT NULL,
  `approved_date` datetime DEFAULT NULL,
  `modi_id` varchar(20) DEFAULT NULL,
  `modi_datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trx_account_type`
--

CREATE TABLE IF NOT EXISTS `trx_account_type` (
  `accounttp_cd` varchar(3) NOT NULL,
  `accounttp_nm` varchar(30) DEFAULT NULL,
  `interest` float DEFAULT NULL,
  `min_saldo_initial` int(11) DEFAULT NULL,
  `adm_cost` int(11) NOT NULL,
  `min_saldo_settles` int(11) NOT NULL,
  `tax` float NOT NULL,
  `modi_id` varchar(20) DEFAULT NULL,
  `modi_datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trx_activity`
--

CREATE TABLE IF NOT EXISTS `trx_activity` (
  `activity_id` bigint(20) NOT NULL,
  `emp_id` bigint(20) NOT NULL,
  `branch_id` bigint(20) NOT NULL,
  `open_balance` int(20) NOT NULL,
  `close_balance` int(20) NOT NULL,
  `trx_balance` int(20) NOT NULL,
  `open_datetime` datetime DEFAULT NULL,
  `close_datetime` datetime DEFAULT NULL,
  `process_user` varchar(20) DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `approved_user` varchar(20) DEFAULT NULL,
  `approved_date` datetime DEFAULT NULL,
  `currency_cd` varchar(20) DEFAULT NULL,
  `modi_id` varchar(20) DEFAULT NULL,
  `modi_datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trx_branch`
--

CREATE TABLE IF NOT EXISTS `trx_branch` (
  `branch_id` bigint(20) NOT NULL,
  `branch_nm` varchar(100) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `region_prop` varchar(100) DEFAULT NULL,
  `region_kab` varchar(100) DEFAULT NULL,
  `region_kec` varchar(100) DEFAULT NULL,
  `region_kel` varchar(100) DEFAULT NULL,
  `postcode` varchar(20) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `branchst_tp` varchar(20) DEFAULT NULL,
  `modi_id` varchar(20) DEFAULT NULL,
  `modi_datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trx_customer`
--

CREATE TABLE IF NOT EXISTS `trx_customer` (
  `cif_id` bigint(20) NOT NULL,
  `ciftp_cdFK` varchar(20) NOT NULL,
  `cif_no` varchar(20) NOT NULL,
  `cif_nm` varchar(50) NOT NULL,
  `gender_tp` varchar(20) NOT NULL,
  `marital_tp` varchar(20) DEFAULT NULL,
  `birth_place` varchar(20) NOT NULL,
  `birth_date` date NOT NULL,
  `npwp_no` varchar(17) DEFAULT NULL,
  `nation_cd` varchar(20) NOT NULL,
  `religion_cd` varchar(20) DEFAULT NULL,
  `address` varchar(200) NOT NULL,
  `rt` varchar(3) NOT NULL,
  `rw` varchar(3) NOT NULL,
  `region_kel` varchar(100) NOT NULL,
  `region_kec` varchar(100) NOT NULL,
  `region_kab` varchar(100) NOT NULL,
  `region_prop` varchar(100) NOT NULL,
  `postcode` varchar(20) NOT NULL,
  `telephone` varchar(14) DEFAULT NULL,
  `phone1` varchar(20) NOT NULL,
  `phone2` varchar(20) DEFAULT NULL,
  `phone3` varchar(15) DEFAULT NULL,
  `education_cd` varchar(20) NOT NULL,
  `occupation_cd` varchar(20) NOT NULL,
  `position` varchar(30) NOT NULL,
  `company_nm` varchar(20) NOT NULL,
  `company_address` varchar(200) NOT NULL,
  `mother_maiden_nm` varchar(30) NOT NULL,
  `file_picture` varchar(50) NOT NULL,
  `income_cd` int(20) NOT NULL,
  `outcome_cd` int(20) NOT NULL,
  `modi_id` varchar(20) DEFAULT NULL,
  `modi_datetime` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trx_customer`
--

INSERT INTO `trx_customer` (`cif_id`, `ciftp_cdFK`, `cif_no`, `cif_nm`, `gender_tp`, `marital_tp`, `birth_place`, `birth_date`, `npwp_no`, `nation_cd`, `religion_cd`, `address`, `rt`, `rw`, `region_kel`, `region_kec`, `region_kab`, `region_prop`, `postcode`, `telephone`, `phone1`, `phone2`, `phone3`, `education_cd`, `occupation_cd`, `position`, `company_nm`, `company_address`, `mother_maiden_nm`, `file_picture`, `income_cd`, `outcome_cd`, `modi_id`, `modi_datetime`) VALUES
(1, 'TPC01', '001', 'Anisa Nia', '', NULL, '', '2018-05-01', NULL, '', NULL, '', '', '', '', '', '', '', '', NULL, '', NULL, NULL, '', '', '', '', '', '', '', 0, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `trx_customer_identity`
--

CREATE TABLE IF NOT EXISTS `trx_customer_identity` (
  `cifid_id` bigint(20) NOT NULL,
  `cif_idFK` bigint(20) NOT NULL,
  `idcard_tp` varchar(20) DEFAULT NULL,
  `id_no` varchar(20) DEFAULT NULL,
  `valid_until` date DEFAULT NULL,
  `modi_id` varchar(20) DEFAULT NULL,
  `modi_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trx_customer_type`
--

CREATE TABLE IF NOT EXISTS `trx_customer_type` (
  `ciftp_cd` varchar(20) NOT NULL,
  `ciftp_nm` varchar(100) DEFAULT NULL,
  `modi_id` varchar(20) DEFAULT NULL,
  `modi_datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trx_customer_type`
--

INSERT INTO `trx_customer_type` (`ciftp_cd`, `ciftp_nm`, `modi_id`, `modi_datetime`) VALUES
('TPC01', 'Perorangan', NULL, NULL),
('TPC02', 'Organisasi/Badan', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `trx_employee`
--

CREATE TABLE IF NOT EXISTS `trx_employee` (
  `emp_id` bigint(20) NOT NULL,
  `emp_no` varchar(10) DEFAULT NULL,
  `emptp_cdFK` varchar(20) NOT NULL,
  `emp_nm` varchar(100) DEFAULT NULL,
  `birth_place` varchar(100) DEFAULT NULL,
  `birth_date` datetime DEFAULT NULL,
  `gender_tp` varchar(20) DEFAULT NULL,
  `file_picture` varchar(50) DEFAULT NULL,
  `modi_id` varchar(20) DEFAULT NULL,
  `modi_datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trx_employee`
--

INSERT INTO `trx_employee` (`emp_id`, `emp_no`, `emptp_cdFK`, `emp_nm`, `birth_place`, `birth_date`, `gender_tp`, `file_picture`, `modi_id`, `modi_datetime`) VALUES
(1, 'E001', 'TPE01', 'Amin', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'E002', 'TPE01', 'Aman', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `trx_emp_job`
--

CREATE TABLE IF NOT EXISTS `trx_emp_job` (
  `job_id` bigint(20) NOT NULL,
  `emp_idFK` bigint(20) NOT NULL,
  `branch_idFK` bigint(20) NOT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `jobst_tp` varchar(20) DEFAULT NULL,
  `time_start` time NOT NULL,
  `time_end` time NOT NULL,
  `modi_id` varchar(20) DEFAULT NULL,
  `modi_datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trx_emp_type`
--

CREATE TABLE IF NOT EXISTS `trx_emp_type` (
  `emptp_cd` varchar(20) NOT NULL,
  `emptp_nm` varchar(100) DEFAULT NULL,
  `modi_id` varchar(20) DEFAULT NULL,
  `modi_datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trx_emp_type`
--

INSERT INTO `trx_emp_type` (`emptp_cd`, `emptp_nm`, `modi_id`, `modi_datetime`) VALUES
('TPE01', 'Tetap', NULL, NULL),
('TPE02', 'Kontrak', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `trx_type`
--

CREATE TABLE IF NOT EXISTS `trx_type` (
  `trxtp_cd` varchar(20) NOT NULL,
  `trxtp_nm` varchar(50) NOT NULL,
  `modi_id` varchar(20) NOT NULL,
  `mod_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `com_code`
--
ALTER TABLE `com_code`
  ADD PRIMARY KEY (`com_cd`);

--
-- Indexes for table `com_log_login`
--
ALTER TABLE `com_log_login`
  ADD PRIMARY KEY (`seq_no`);

--
-- Indexes for table `com_menu`
--
ALTER TABLE `com_menu`
  ADD PRIMARY KEY (`menu_cd`);

--
-- Indexes for table `com_role`
--
ALTER TABLE `com_role`
  ADD PRIMARY KEY (`role_cd`);

--
-- Indexes for table `com_role_menu`
--
ALTER TABLE `com_role_menu`
  ADD PRIMARY KEY (`role_menu_id`),
  ADD KEY `menu_cdFK` (`menu_cdFK`),
  ADD KEY `role_cdFK` (`role_cdFK`);

--
-- Indexes for table `com_user`
--
ALTER TABLE `com_user`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `role_cdFK` (`role_cdFK`);

--
-- Indexes for table `trx_account`
--
ALTER TABLE `trx_account`
  ADD PRIMARY KEY (`account_id`),
  ADD KEY `accounttp_cdFK` (`accounttp_cdFK`),
  ADD KEY `cif_idFK` (`cif_idFK`);

--
-- Indexes for table `trx_account_trx`
--
ALTER TABLE `trx_account_trx`
  ADD KEY `account_idFK` (`account_idFK`);

--
-- Indexes for table `trx_account_type`
--
ALTER TABLE `trx_account_type`
  ADD PRIMARY KEY (`accounttp_cd`);

--
-- Indexes for table `trx_branch`
--
ALTER TABLE `trx_branch`
  ADD PRIMARY KEY (`branch_id`);

--
-- Indexes for table `trx_customer`
--
ALTER TABLE `trx_customer`
  ADD PRIMARY KEY (`cif_id`),
  ADD KEY `cistp_cd` (`ciftp_cdFK`);

--
-- Indexes for table `trx_customer_identity`
--
ALTER TABLE `trx_customer_identity`
  ADD PRIMARY KEY (`cifid_id`),
  ADD KEY `cif_idFK` (`cif_idFK`),
  ADD KEY `cif_idFK_2` (`cif_idFK`);

--
-- Indexes for table `trx_customer_type`
--
ALTER TABLE `trx_customer_type`
  ADD PRIMARY KEY (`ciftp_cd`);

--
-- Indexes for table `trx_employee`
--
ALTER TABLE `trx_employee`
  ADD PRIMARY KEY (`emp_id`),
  ADD KEY `trx_employee_ibfk_1` (`emptp_cdFK`);

--
-- Indexes for table `trx_emp_job`
--
ALTER TABLE `trx_emp_job`
  ADD KEY `emp_idFK` (`emp_idFK`),
  ADD KEY `branch_idFK` (`branch_idFK`);

--
-- Indexes for table `trx_emp_type`
--
ALTER TABLE `trx_emp_type`
  ADD PRIMARY KEY (`emptp_cd`);

--
-- Indexes for table `trx_type`
--
ALTER TABLE `trx_type`
  ADD PRIMARY KEY (`trxtp_cd`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `com_log_login`
--
ALTER TABLE `com_log_login`
  MODIFY `seq_no` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `com_role_menu`
--
ALTER TABLE `com_role_menu`
  MODIFY `role_menu_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `trx_account`
--
ALTER TABLE `trx_account`
  MODIFY `account_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `trx_customer`
--
ALTER TABLE `trx_customer`
  MODIFY `cif_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `trx_customer_identity`
--
ALTER TABLE `trx_customer_identity`
  MODIFY `cifid_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `com_user`
--
ALTER TABLE `com_user`
  ADD CONSTRAINT `com_user_ibfk_1` FOREIGN KEY (`role_cdFK`) REFERENCES `com_role` (`role_cd`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `trx_account`
--
ALTER TABLE `trx_account`
  ADD CONSTRAINT `trx_account_ibfk_1` FOREIGN KEY (`accounttp_cdFK`) REFERENCES `trx_account_type` (`accounttp_cd`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `trx_account_ibfk_2` FOREIGN KEY (`cif_idFK`) REFERENCES `trx_customer` (`cif_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `trx_account_trx`
--
ALTER TABLE `trx_account_trx`
  ADD CONSTRAINT `trx_account_trx_ibfk_1` FOREIGN KEY (`account_idFK`) REFERENCES `trx_account` (`account_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `trx_customer`
--
ALTER TABLE `trx_customer`
  ADD CONSTRAINT `trx_customer_ibfk_1` FOREIGN KEY (`ciftp_cdFK`) REFERENCES `trx_customer_type` (`ciftp_cd`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `trx_customer_identity`
--
ALTER TABLE `trx_customer_identity`
  ADD CONSTRAINT `trx_customer_identity_ibfk_1` FOREIGN KEY (`cif_idFK`) REFERENCES `trx_customer` (`cif_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `trx_employee`
--
ALTER TABLE `trx_employee`
  ADD CONSTRAINT `trx_employee_ibfk_1` FOREIGN KEY (`emptp_cdFK`) REFERENCES `trx_emp_type` (`emptp_cd`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `trx_emp_job`
--
ALTER TABLE `trx_emp_job`
  ADD CONSTRAINT `trx_emp_job_ibfk_1` FOREIGN KEY (`branch_idFK`) REFERENCES `trx_branch` (`branch_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `trx_emp_job_ibfk_2` FOREIGN KEY (`emp_idFK`) REFERENCES `trx_employee` (`emp_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
